#ifndef RECVCONNECT_H
#define RECVCONNECT_H

#include <QObject>
#include <QDebug>
#include <QTcpSocket>
#include <QTcpServer>
#include <QAbstractSocket>
class RecvConnect : public QObject
{
    Q_OBJECT
public:
    explicit RecvConnect(QObject *parent = 0);

signals:

public slots:
    void newConnection();
    void startRead();
private:
    QTcpServer *server;
     QTcpSocket *socketServer;
};

#endif // RECVCONNECT_H
