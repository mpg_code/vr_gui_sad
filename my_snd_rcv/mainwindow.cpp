#include "mainwindow.h"
#include "ui_mainwindow.h"


#include <QFile>
#include <QJsonObject>
#include <QJsonDocument>
#include <QVariant>

#include <QDebug>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    socket = new QTcpSocket(this);
    connect(socket,SIGNAL(connected()),this,SLOT(connected()));
    connect(socket,SIGNAL(readyRead()),this,SLOT(readyRead()));
    connect(socket,SIGNAL(bytesWritten(qint64)),this,SLOT(bytesWritten(qint64)));
    //connect(socket,SIGNAL(connected()),this,SLOT(sendString(qint64)));

    qDebug() << "Connecting..." ;
    socket->connectToHost("localhost",54126);
    if(!socket->waitForConnected(1000))
    {
            qDebug() << "Client Error: " <<socket->errorString();
    }
//socket->write("test");

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::Test()
{

}

void MainWindow::connected()
{
    qDebug() << "Client Connected" ;
    //socket->write("hello");

}
void MainWindow::disconnected()
{
connect(socket,SIGNAL(disconnected()),this,SLOT(disconnected()));
    qDebug() << "Client Disconnected" ;
    socket->close();
}

void MainWindow::bytesWritten(qint64 bytes)
{
    qDebug() << "Client wrote: " << bytes;
}

void MainWindow::readyRead()
{
    qDebug() << "Client Reading..." ;
    qDebug() << socket->readAll();
}

void MainWindow::sendString()
{
    // make a JSON string from  sliders
    connect(socket, SIGNAL(connected()),this, SLOT(sendString()));
    qDebug() << "Sending..." ;


//    QString slider1,slider2,slider1_json,slider2_json,mJanssonString;
//    slider1 = QString::number(ui->horizontalSlider->value());
//    slider2 = QString::number(ui->horizontalSlider_2->value());
//    slider1_json = "{slider1:"+slider1+"}";
//    slider2_json = "{slider2:"+slider2+"}";
//    mJanssonString = "["+slider1_json+","+slider2_json+"]";
//    qDebug() << mJanssonString;

    //qDebug() << mJanssonString;



//    QVariantMap map;
//        map.insert("Slider1", ui->horizontalSlider->value());
//        map.insert("Slider2", ui->horizontalSlider_2->value());
//     QJsonObject object = QJsonObject::fromVariantMap(map);

//     QJsonDocument document;
//     document.setObject(object);

//     QString fileName="jsonString.json";
//        QFile jsonFile(fileName);

//        jsonFile.open(QFile::WriteOnly);
//        jsonFile.write(document.toJson());

//        QFile file("jsonString.json");
//        file.open(QIODevice::ReadOnly);
//        QByteArray rawData = file.readAll();




    QJsonObject allSliders;
    allSliders["slider1"] = ui->horizontalSlider->value();
    allSliders["slider2"] = ui->horizontalSlider_2->value();

    QJsonDocument doc(allSliders);
    QByteArray bytes = doc.toJson();

    qDebug() << "bytes:" << bytes;
    socket->write(bytes);
    socket->flush();
    //socket->waitForBytesWritten(3000);

}

void MainWindow::on_pushButton_clicked()
{

    //connect(socket, SIGNAL(connected()),this, SLOT(sendString()));

    // send JSON string to receiver
    sendString();


}




void MainWindow::on_pushButton_2_clicked()
{
        socket->close();

}
