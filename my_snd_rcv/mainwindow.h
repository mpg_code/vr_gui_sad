#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QObject>
#include <QTcpSocket>
#include <QDebug>
#include <QAbstractSocket>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void Test();
/*
    QString name() const;
    void setName(const QString &name);

    int value() const;
    void setvaluel(int value);

    enum SaveFormat {
        Json, Binary
    };

    bool saveJsonObject(SaveFormat saveFormat) const;

    void read(const QJsonObject &json);
    void write(QJsonObject &json) const;
*/
private slots:
//    QString mName;
//    int mValue;

    void on_pushButton_clicked();

    void connected();
    void disconnected();

    void bytesWritten(qint64 bytes);

    void readyRead();
    void sendString();

    void on_pushButton_2_clicked();

private:
    Ui::MainWindow *ui;
    QTcpSocket *socket;
    QString mJanssonString;
};

#endif // MAINWINDOW_H
